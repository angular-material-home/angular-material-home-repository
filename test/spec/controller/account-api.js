'use strict';

describe('MbAccountCtrl', function() {

	// load the controller's module
	beforeEach(module('ngMaterialHome'));

	var ContentCtrl, scope;
	var routeParams = {
			name : 'contentname'
	};
	var translate = {
			use : function() {
			}
	};
	var rootScope = {
			$on : function() {
			}
	};

	// Initialize the controller and a mock scope
	beforeEach(inject(function($controller, $rootScope, _$menu_, _$cms_) {

		scope = $rootScope.$new();
		ContentCtrl = $controller('MbAccountCtrl', {
			$scope : scope,
			// place here mocked dependencies
			$menu : _$menu_,
			$cms : _$cms_,
			$routeParams : routeParams,
			$translate : translate,
			$rootScope : rootScope
		});
	}));

	it('should attach a list of awesomeThings to the scope', function() {
		// expect(ContentCtrl.awesomeThings.length).toBe(3);
	});
});
